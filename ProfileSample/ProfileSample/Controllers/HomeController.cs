﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ProfileSample.DAL;
using ProfileSample.Models;

namespace ProfileSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new List<ImageModel>();

            using (var context = new ProfileSampleEntities())
            {
                var sources = context.ImgSources.Take(20).ToList();

                foreach (var item in sources)
                {
                    model.Add(new ImageModel
                    {
                        Name = item.Name,
                        Data = item.Data
                    });
                }
            }

            return View(model);
        }

        public ActionResult Convert()
        {
            var files = Directory.GetFiles(Server.MapPath("~/Content/Img"), "*.jpg");

            using (var context = new ProfileSampleEntities())
            {
                foreach (var file in files)
                {
                    using (var stream = new FileStream(file, FileMode.Open))
                    {
                        byte[] buff = new byte[stream.Length];

                        stream.Read(buff, 0, (int)stream.Length);

                        context.ImgSources.Add(new ImgSource
                        {
                            Name = Path.GetFileName(file),
                            Data = buff
                        });
                    }
                }
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}